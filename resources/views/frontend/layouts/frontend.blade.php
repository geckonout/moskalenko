<!DOCTYPE html>
<!--[if IE 9]>
<html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Moskalenko</title>

    <meta name="description" content="Moskalenko blog">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

    <link rel="shortcut icon" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <link rel="stylesheet" href="assets/css/plugins.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/themes.css">
    <script src="assets/js/vendor/modernizr-3.3.1.min.js"></script>
</head>
<body>

<div id="page-container" class="boxed">
    <section class=" themed-background-dark-default">
        <img class="img-responsive" src="assets/img/main2.png" alt="">
    </section>

    <section class="site-section site-content border-bottom overflow-hidden">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 push">
                    <h2 class="site-heading"><strong>Кто</strong></h2>
                    <p class="feature-text text-muted push text-small"><strong>Москаленко Яна.</strong> <br>Несмотря на
                        то, что её
                        трудовые будни вот уже который год вращаются вокруг международных отношений да кафедры
                        скандинавских
                        языков, сердцем она по-прежнему верна всему, что связано с Испанией и классической литературой.
                        Верит в существование оборотней и отчаянно надеется, что однажды в полнолуние и ей повезёт.
                        Гордый обладатель «эмоционального диапазона, как у зубочистки», но на самом деле натура весьма
                        впечатлительная.
                        Черпает вдохновение из мелочей. Страдает аллергией на корицу и счастливые концовки в
                        произведениях.
                        Некогда стеснялась своей любви к кладбищенским оттенкам в текстах,
                        однако теперь сию манеру любовно списывает на наследие Эдгара По. Viva Drama!
                    </p>
                </div>
                <div class="col-sm-6 clearfix push">
                    <img src="assets/img/my.jpg" alt=""
                         class="img-responsive center-block visibility-none" data-toggle="animation-appear"
                         data-animation-class="animation-fadeInLeft"
                         style="max-width: 450px">
                </div>
            </div>
        </div>
    </section>

    <footer class="site-footer site-section site-section-light">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h4 class="footer-heading">Напиши</h4>
                    <form action="index.html" method="post" class="form-inline" onsubmit="return false;">
                        <div class="form-group text-center">
                            <input type="email" id="register-email" name="register-email" class="form-control"
                                   placeholder="Your Email.."><br>
                            <input type="email" id="register-email" name="register-email" class="form-control"
                                   placeholder="Your Email.."> <br>
                            <input type="email" id="register-email" name="register-email" class="form-control"
                                   placeholder="Your Email.."> <br>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-primary">Subscribe</button>
                                </span>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="col-sm-6">
                    <h4 class="footer-heading">Найди</h4>

                    <ul class="footer-nav footer-nav-links list-inline">
                        <li><a href="javascript:void(0)" class="social-facebook" data-toggle="tooltip"
                               title="Facebook"><i class="fa fa-fw fa-facebook"></i></a></li>
                        <li><a href="javascript:void(0)" class="social-twitter" data-toggle="tooltip"
                               title="VK"><i class="fa fa-fw fa-vk"></i></a></li>
                        <li><a href="javascript:void(0)" class="social-dribbble" data-toggle="tooltip"
                               title="Instagram"><i class="fa fa-fw fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</div>

<a href="#" id="to-top"><i class="fa fa-arrow-up"></i></a>
<script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
<script src="assets/js/vendor/bootstrap.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/app.js"></script>
</body>
</html>